const Signature = artifacts.require("Signature");

contract('Signature Test', async accounts => {
    it("isSubscriber", async () => {
        var msg = new Buffer('Add me as a subscriber.');
        msg = '0x' + msg.toString('hex')
        var from = "0x5441bf9aa27e79c7aa4a8d1b340d16973695dee1"
        var signature = "0xc87c42573dc6c76de427dcad62bb09ab71de140c79edb773e47cb0bb1cc2cc7754927a50a641f3eb0850a67f2fc512b14f8fec665056342503a2c357b149886b1c"
        signature = signature.substr(2); //remove 0x
        const r = '0x' + signature.slice(0, 64)
        const s = '0x' + signature.slice(64, 128)
        const v = '0x' + signature.slice(128, 130)
        const v_decimal = web3.utils.toDecimal(v)
        
        console.log(signature, v, v_decimal, r, s)

        let instance = await Signature.deployed();
        let value = await instance.recoverAddr.call(msg, v_decimal, r, s);

        assert.equal(
            value,
            from,
            "Amount wasn't correctly taken from the sender"
        );
    });
});
