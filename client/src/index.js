const Web3 = require("web3") // import web3 v1.0 constructor

window.addEventListener('load', async () => {
    // Modern dapp browsers...
    if (window.ethereum) {
        window.web3 = new Web3(ethereum);
        try {
            // Request account access if needed
            await ethereum.enable();
            // Acccounts now exposed
        } catch (error) {
            // User denied account access...
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);
        // Acccounts always exposed
    }
    // Non-dapp browsers...
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }
});

personalSignButton.addEventListener('click', function(event) {
    event.preventDefault()
    var msg = new Buffer('Add me as a subscriber.');
    msg = '0x' + msg.toString('hex')
    var from = web3.currentProvider.connection.selectedAddress
    if (!from) return connect()

    var params = [msg, from]
    var method = 'personal_sign'

    /*var abi = [{"constant":true,"inputs":[{"name":"msgHash","type":"bytes32"},{"name":"v","type":"uint8"},{"name":"r","type":"bytes32"},{"name":"s","type":"bytes32"}],"name":"recoverAddr","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"pure","type":"function","signature":"0xe5df669f"},{"constant":true,"inputs":[{"name":"_addr","type":"address"},{"name":"msgHash","type":"bytes32"},{"name":"v","type":"uint8"},{"name":"r","type":"bytes32"},{"name":"s","type":"bytes32"}],"name":"isSigned","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"pure","type":"function","signature":"0x8677ebe8"}]
    var signatureContract = new web3.eth.Contract(abi)
    var signature = signatureContract.at("0x88391A95aCE612Edc1505282F27FC157587eb73d");
    console.log(signature)*/

    web3.currentProvider.connection.sendAsync({
        method,
        params,
        from,
    }, function (err, result) {
        if (err) return console.error(err)
        if (result.error) return console.error(result.error)
        var signature = result.result
        signature = signature.substr(2); //remove 0x
        const r = '0x' + signature.slice(0, 64)
        const s = '0x' + signature.slice(64, 128)
        const v = '0x' + signature.slice(128, 130)
        const v_decimal = web3.utils.toDecimal(v)

        console.log(signature, v, v_decimal, r, s) 
    })
  })

function toHex(str) {
    var hex = ''
    for(var i=0;i<str.length;i++) {
        hex += ''+str.charCodeAt(i).toString(16)
    }
    return hex
}